package com.poo.sorteio.api.entity;


import jakarta.persistence.*;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.Data;
import org.hibernate.annotations.JoinColumnOrFormula;

import java.util.List;

@Data
@Entity
@Table(name = "tb_sorteio")
public class Sorteio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer ano;

    private Integer mes;

    @Column(name = "valor_premio")
    private Integer valorPremio;

    private Integer apresentador;

    private Integer auditor;

    private String descricao;

    private String tipo;

    @ManyToOne
    @JoinColumn(name = "identificador")
    private Usuario usuarioId;

    @ManyToOne
    @JoinColumn(name = "premiado")
    private Bilhete bilhetes;
}
