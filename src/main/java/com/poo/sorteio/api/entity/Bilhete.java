package com.poo.sorteio.api.entity;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.mapping.ToOne;

import java.util.List;
import java.util.Scanner;

@Data
@Entity
@Table(name = "tb_bilhete")
public class Bilhete {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer ano;

    private Integer mes;

    private Integer numero;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuarioId;

    @OneToOne
    @JoinColumn(name = "nota_fiscal_id")
    private NotaFiscal notaFiscal;

    @OneToMany(mappedBy = "bilhetes")
    private List<Sorteio> sorteio;

}

