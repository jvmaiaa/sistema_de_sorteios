package com.poo.sorteio.api.entity;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.mapping.ToOne;

import java.time.LocalDate;

@Data
@Entity
@Table(name = "tb_nota_fiscal")
public class NotaFiscal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String chave;

    private String sequencial;

    @Column(name = "data_processamento")
    private LocalDate dataProcessamento;

    @ManyToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuarioId;

    @OneToOne(mappedBy = "notaFiscal")
    private Bilhete bilhete;

}
