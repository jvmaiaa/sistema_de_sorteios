package com.poo.sorteio.api.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "tb_usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean consentimento;

    @Column(name = "data_nascimento")
    private LocalDate dataDeNascimento;

    private String nome;

    private String cpf;

    private String email;

    private String perfil;

    private String role;

    private String senha;

    private String telefone;

    private String estado;

    private String municipio;

    private String cep;

    private String bairro;

    private String logradouro;

    private String numero;

    private String complemento;

    @OneToMany(mappedBy = "usuarioId")
    private List<NotaFiscal> notaFiscal;

    @OneToMany(mappedBy = "usuarioId")
    private List<Bilhete> bilhete;

    @OneToMany(mappedBy = "usuarioId")
    private List<Sorteio> sorteio;

}
