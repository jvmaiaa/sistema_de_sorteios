# Sobre o projeto
O projeto consiste no cadastro de usuários, e a associação de vários bilhetes (premiados ou não) para um usuário. A partir disso, será feito o sorteio que contém vários usuários e **apenas um bilhete premiado**. Utilizei o PostgreSQL para realizar os testes de relacionamentos das minhas entidades.

## MER - Modelo Entidade Relacionamento
-> Abaixo, segue o MER que foi utilizado como base para realizar os relacionamentos das entidades presentes na aplicação.

![alt text](image.png)

## Tecnologias
- Spring Boot 3
- PostresSQL
- Spring Data JPA
- Hibernate
- JDK 17

# Como Rodar
Para Rodar o projeto, basta criar um banco de dados Postgres na sua máquina usando o `PostgreSQL` e configurar o nome do seu Banco de Dados no arquivo `application.properties` com o nome do Banco criado. Com isso, pode realizar o `run` da aplicação por alguma IDE `(eclipse IDE, IntelliJ IDEA, NetBeans, etc)`.

0BS: O nome padrão do Banco do projeto é `sorteio`.